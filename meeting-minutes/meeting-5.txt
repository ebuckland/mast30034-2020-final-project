## MAST30034 Final Project Group 19

### Meeting Minutes
October 8, 2020

### Opening
Fifth meeting for project proposal

### Present
Edward Buckland,
David Liu,
Lawrence Ma,
Ari Boyd,

### Absent
None

### Business from Previous Meeting
* Doc2vec of tweets
* Merging with tweets and emission data

### Status Update
* Edward has de-emojied the tweets, plans to clean up the datasets
* Lawrence demonstrates code to doc2vec the tweets, uploads to repo
* Joining of datasets is reassigned to David
* GLM initial testing assigned to Ari

### Plan
* Week 10 - merging datasets and train models (GLM, SVM and neural network(perhaps use random forest instead))
* Week 11 - do video/train more models
* Week 12 - finish report
* Join datasets
* Train GLM
