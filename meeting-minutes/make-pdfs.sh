#!/bin/bash
touch make-pdfs.py

echo "from weasyprint import *" >> make-pdfs.py
echo "from weasyprint.document import *" >> make-pdfs.py
echo "from glob import *" >> make-pdfs.py
echo "pages = []" >> make-pdfs.py
echo "for file in sorted(glob('*.html')):" >> make-pdfs.py
echo "    for page in HTML(file).render().pages:" >> make-pdfs.py
echo "        pages.append(page)" >> make-pdfs.py
echo "output = open('meeting-minutes.pdf', 'wb').write(Document(pages, DocumentMetadata(), None, None).write_pdf())" >> make-pdfs.py

chmod +x make-pdfs.py

for file in *.txt
do
    newfile=`echo $file | sed -e 's/txt$/html/'`
    pandoc $file -o $newfile --wrap=preserve
done

python3 make-pdfs.py

rm *.py *.html